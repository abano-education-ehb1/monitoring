import queue
import pika
from datetime import datetime
import os
def send_error(level, message):
    credentials = pika.PlainCredentials(os.getenv('RABBIT_USER'), os.getenv('RABBIT_PASSWORD'))
    connection = pika.BlockingConnection(pika.ConnectionParameters(os.getenv('VM'), os.getenv('RABBIT_PORT'), '/', credentials))
    channel = connection.channel()
    print('Level: '+ level)
    print('Message: '+ str(message))
    time = datetime.now().timestamp()
    remove = str(message).replace('<', '')
    remove2 = str(remove).replace('>', '')    
    msgXML = """
        <error>
            <source>monitoring</source>
            <date>{}</date>
            <level>{}</level>
            <message>{}</message>
        </error>
    """.format(int(time), level, str(remove2))
    channel.queue_declare('errors')
    channel.basic_publish(
        exchange = '',
        routing_key = 'errors',
        body= msgXML
    )
    print('Xml SENT')
    connection.close()