import os
from lxml import etree
from heartbeat import Heartbeat
from datetime import datetime, timezone
from sender import send_error
xml_heartbeat = """
    <heartbeat>
        <source>frontend</source>
        <date>1652102926</date>
    </heartbeat>
"""
xml_user = """
<user>
  <source>frontend</source>
  <source-id>string</source-id>
  <uuid>kjslkfqsjlfkq</uuid>
  <action>update</action>
  <properties>
    <firstname>Hich</firstname>
    <lastname>Am</lastname>
    <email>hicham@gmail.com</email>
    <address>
      <street>string</street>
      <housenumber>46o35</housenumber>
      <postalcode>1802</postalcode>
      <city>string</city>
      <country>string</country>
    </address>
    <phone>901290</phone>
    <company>
      <source-id>string</source-id>
      <uuid>string</uuid>
    </company>
  </properties>
</user>
"""

heartbeat_file = '/logs/heartbeats.log'
user_file = '/logs/users.log' 

def check_xml(xml_string):
    xml_file = etree.fromstring(xml_string)
    xml_validator_heartbeat = etree.XMLSchema(file="./resources/heartbeat.xsd")
    xml_validator_user = etree.XMLSchema(file="./resources/user.xsd")
    if xml_validator_heartbeat.validate(xml_file):
        log_heartbeat(xml_string=xml_string) 
    elif xml_validator_user(xml_file):
        log_user(xml_string=xml_string)
    else:
        level = 'error'
        message = 'XML is not valid'
        send_error(level=level, message=message)
        print('send error')    

def log_user(xml_string):
    unix_time = datetime.utcfromtimestamp(
        int(datetime.now().timestamp())
    ).strftime('%d-%m-%Y %H:%M:%S')
    user = {'action': '', 'uuid': '', 'firstname': '', 'lastname': '', 'email': '', 'phone': ''}
    root = etree.fromstring(xml_string)
    for tags in root:
        if user.keys().__contains__(tags.tag):
            user[tags.tag] = tags.text
        for elems in tags:
            if user.keys().__contains__(elems.tag):
                user[elems.tag] = elems.text
    with open(user_file, 'a') as file:
        file.write("""{},{},[{}],{},{},{} \n""".format(
            unix_time,
            user['uuid'],
            user['action'].upper(),
            user['firstname'], 
            user['lastname'], 
            user['email']))

def log_heartbeat(xml_string):
    result = []
    root = etree.fromstring(xml_string)
    for tags in root:
        result.append(tags.text)
    with open(heartbeat_file, 'a') as file:
        if os.path.getsize(heartbeat_file) > 1000:
            file.truncate(0)
            print('Content of heartbeat file deleted')
        unix_time = datetime.utcfromtimestamp(int(result[1])).strftime('%d-%m-%Y %H:%M:%S')
        file.write("""{},[HEARTBEAT],[{}] \n""".format(unix_time, result[0].upper()))

#try:
#    check_xml(xml_string=xml_user) 
#except Exception as e:
#    print('Send error')