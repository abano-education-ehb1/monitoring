from operator import truediv
import unittest
import XMLparser
from lxml import etree

# run 'python -m unittest -v test_heartbeat.py' to run test
# remove '-v' for simplified output of the test. Keep for more detailed

class TestXML(unittest.TestCase):

    def test_check_heartbeat_xml_valid(self):
        # below is a correct XML file converted from string and thus will throw a true return boolean
     xml_validator_heartbeat = etree.XMLSchema(file="./resources/heartbeat.xsd")
     xml_string_example = '<heartbeat>  <source>frontend</source>    <date>1652102926</date> </heartbeat>'
     raw_xml = etree.fromstring(xml_string_example)

     self.assertEqual(xml_validator_heartbeat.validate(raw_xml), True)

    def test_check_heartbeat_xml_invalid(self):
        # below is an incorrect XML file converted from string and thus will throw a false return boolean
     xml_validator_heartbeat = etree.XMLSchema(file="./resources/heartbeat.xsd")
     xml_string_example = '<heartbeat>  <source>frontend</source>    <date>03-JAN-20</date> </heartbeat>'
     raw_xml = etree.fromstring(xml_string_example)

     self.assertEqual(xml_validator_heartbeat.validate(raw_xml), False)

    def test_check_user_xml_valid(self):
        # below is a correct XML file converted from string and thus will throw a true return boolean
     xml_validator_user = etree.XMLSchema(file="./resources/user.xsd")    
     xml_string_example = """
<user>
  <source>frontend</source>
  <source-id>string</source-id>
  <uuid>string</uuid>
  <action>update</action>
  <properties>
    <firstname>string</firstname>
    <lastname>string</lastname>
    <email>string</email>
    <address>
      <street>string</street>
      <housenumber>4635</housenumber>
      <postalcode>1802</postalcode>
      <city>string</city>
      <country>string</country>
    </address>
    <phone>string</phone>
    <company>
      <source-id></source-id>
      <uuid></uuid>
    </company>
  </properties>
</user>
"""
     raw_xml = etree.fromstring(xml_string_example)

     self.assertEqual(xml_validator_user.validate(raw_xml), True)


    def test_check_user_xml_invalid(self, ):
       # below is an incorrect XML file converted from string and thus will throw a false return boolean
     xml_validator_user = etree.XMLSchema(file="./resources/user.xsd")    
     xml_string_example = """
<user>
  <source>frontend</source>
  <source-id>string</source-id>
  <uuid>kjslkfqsjlfkq</uuid>
  <action>update</action>
  <properties>
    <firstname>Hich</firstname>
    <lastname>Am</lastname>
    <email>hicham@gmail.com</email>
    <phone>901290</phone>
    <company>
      <source-id>string</source-id>
      <uuid>string</uuid>
    </company>
  </properties>
</user>
"""
     raw_xml = etree.fromstring(xml_string_example)

     self.assertEqual(xml_validator_user.validate(raw_xml), False)



if __name__ == '__main__':
    unittest.main()