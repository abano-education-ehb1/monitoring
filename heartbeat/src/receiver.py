import pika
from XMLparser import check_xml
from sender import send_error
import os

if not os.path.exists('/logs/heartsbeats.log'):
    os.system('touch /logs/heartbeats.log')

if not os.path.exists('/logs/users.log'):
    os.system('touch /logs/users.log')    

credentials = pika.PlainCredentials(os.getenv('RABBIT_USER'), os.getenv('RABBIT_PASSWORD'))
connection = pika.BlockingConnection(pika.ConnectionParameters(os.getenv('VM'), os.getenv('RABBIT_PORT'), '/', credentials))
channel = connection.channel()

queue1 = channel.queue_declare('monitoring')
queue1_name = queue1.method.queue

def callback(ch, method, properties, body):
    print('Generating message...')
    try:
        check_xml(xml_string=body)
    except Exception as e:
        send_error(level='error',message=e)

channel.basic_consume(on_message_callback=callback, queue=queue1_name, auto_ack=True)
print(' Waiting for messages (CTRL+C to stop)')
channel.start_consuming()