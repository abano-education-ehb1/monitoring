from datetime import datetime
import pika
from errorXMLparser import check_xml
import logging
import os

credentials = pika.PlainCredentials(os.getenv('RABBIT_USER'), os.getenv('RABBIT_PASSWORD'))
connection = pika.BlockingConnection(pika.ConnectionParameters(os.getenv('VM'), os.getenv('RABBIT_PORT'), '/', credentials))
channel = connection.channel()
queue1 = channel.queue_declare('errors')
queue1_name = queue1.method.queue

error_file = '/logs/errors.log'
testing_file = '/logs/testing.log'

if not os.path.exists('/logs/errors.log'):
    os.system('touch /logs/errors.log')
if not os.path.exists('/logs/testing.log'):
    os.system('touch /logs/testing.log')
    
def callback(ch, method, properties, body):
    try:
        check_xml(body)
    except Exception as e:
        #new_string = body.decode()
        new_string = str(body).replace('\n', '')
        new_string.replace('\r', '')
        new_string.replace(' ', '')
        logging.basicConfig(format='%(message)s')
        logging.error('XML RECEIVED IS NOT VALID! ' + new_string)
        with open(error_file, 'a') as file:
            file.write('[ERROR]£ [MONITORING]£ ERROR XML IS NOT VALID ' + new_string + '\n') 
        
channel.basic_consume(on_message_callback=callback, queue=queue1_name, auto_ack=True)
print(' Waiting for messages (CTRL+C to stop)')
channel.start_consuming()