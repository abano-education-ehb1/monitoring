from lxml import etree
from datetime import datetime
import logging
import os
xml_string = """
    <error>
        <source>frontend</source>
        <date>1650968324</date>
        <level>fatal</level>
        <message>This is an error</message>
    </error>
"""

error_file = '/logs/errors.log'

def check_xml(xml_string):

    xml_file = etree.fromstring(xml_string)
    xml_validator = etree.XMLSchema(file="./error.xsd")
    if xml_validator.validate(xml_file):
        print("XML IS VALID")
        if os.path.exists('/logs/errors.log'):
            log_error(xml_string=xml_string) 
        
        return True
    else:
        #new_string = xml_string.decode()
        new_string = str(xml_string).replace('\n', '')
        new_string.replace('\r', '')
        new_string.replace(' ', '')
        logging.basicConfig(format='%(message)s')
        logging.error("XML IS NOT VALID! " + new_string)
        if os.path.exists('/logs/errors.log'):
            with open(error_file, 'a') as file:
                file.write('[ERROR]£ [MONITORING]£ ERROR XML IS NOT VALID ' + new_string + '\n')
        return False
        

def log_error(xml_string):
    result = []
    root = etree.fromstring(xml_string)
    for tags in root:
        result.append(tags.text)
    #logging.basicConfig(format='%(message)s') 
    #logging.error("""[{}], [{}], {}""".format(result[2].upper(), result[0].upper(), result[3]))    
    with open(error_file, 'a') as file:
            file.write("""[{}]£ [{}]£ {} \n""".format(result[2].upper(), result[0].upper(), result[3]))

#if check_xml(xml_string=xml_string):
#    log_error(xml_string)
#print(etree.tostring(store, encoding='UTF-8', xml_declaration=True))

#check_xml(xml_string=xml_string)