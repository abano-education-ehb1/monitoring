import unittest
import errorXMLparser

# run 'python -m unittest -v test_error-logging.py' to run test
# remove '-v' for simplified output of the test. Keep for more detailed

class TestXML(unittest.TestCase):


    def test_check_xml_valid(self):
        # below is a correct XML string and thus will throw a true return boolean
     xml_string_example = '<error> <source>frontend</source> <date>1650968324</date> <level>fatal</level>  <message>This is an error</message> </error>'
     self.assertTrue(errorXMLparser.check_xml(xml_string_example))

    def test_check_xml_invalid(self):
        # below is a wrong XML string and thus will throw a false return boolean
     xml_string_example = '<error> <source>Intruder</source> <date>1650968324</date> <level>fatal</level>  <message>We are getting in!</message> </error>'
     self.assertFalse(errorXMLparser.check_xml(xml_string_example))


if __name__ == '__main__':
    unittest.main()